let {MediaType} = global.SnappFramework;

module.exports = function (self) {
  self.mediaType = this.optional.MediaType || MediaType.text;
  self.status = this.integerRange(100, 599);
  self.response.writeHead(self.status, {
    'Content-Type': self.mediaType.type
  });
  self.response.end(this.string);
};
